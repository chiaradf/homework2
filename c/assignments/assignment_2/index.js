
const FIRST_NAME = "Chiara";
const LAST_NAME = "de Feijter";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache={};
    cache.about=0;
    cache.home=0;
    cache.contact=0;
    cache.pageAccessCounter=function(pg="HOME"){
        if(pg===undefined){
            this.home++; 
        }
         else{
            if(pg.toUpperCase()==="ABOUT"){
                this.about++;
                }
            if(pg.toUpperCase()==="HOME"){
                this.home++;
                }
            if(pg.toUpperCase()==="CONTACT"){
                this.contact++;
                }    
            }
    };
    cache.getCache=function(){
        return cache;
    }
    return cache;
    
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}


